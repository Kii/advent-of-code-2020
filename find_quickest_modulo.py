from io_helper import read_file_as_lines
from math_helper import sync_modulo, get_aggregate_period_and_offset


def get_buses(line):
    all_buses = []
    for bus_info in line.split(","):
        all_buses.append(bus_info)
    return all_buses


def get_max_bus(all_buses):
    idx = 0
    max_bus = -1
    for i in range(len(all_buses)):
        bus = all_buses[i]
        if bus != "x":
            if int(bus) > max_bus:
                idx = i
                max_bus = int(bus)
    return idx, max_bus


def get_min_bus(all_buses):
    idx = 0
    min_bus = 1000000000
    for i in range(len(all_buses)):
        bus = all_buses[i]
        if bus != "x":
            if int(bus) < min_bus:
                idx = i
                min_bus = int(bus)
    return idx, min_bus


def get_offset_dict(all_buses):
    offset_dict = dict()
    for i in range(len(all_buses)):
        bus = all_buses[i]
        if bus != "x":
            offset_dict[int(bus)] = i

    return offset_dict


def is_good_bus(time, bus_number):
    return time % bus_number == 0


def get_start_sync(bus_offset_dict):
    bus1 = list(bus_offset_dict.keys())[0]
    bus2 = list(bus_offset_dict.keys())[1]

    bus_aggregation_period, bus_aggregation_offset = \
        get_aggregate_period_and_offset(bus1, bus_offset_dict[bus1],
                                        bus2, bus_offset_dict[bus2])

    for i in range(1, len(bus_offset_dict.keys()) - 1):
        other_bus_period = list(bus_offset_dict.keys())[i+1]
        other_bus_offset = bus_offset_dict[other_bus_period]
        bus_aggregation_period, bus_aggregation_offset = \
            get_aggregate_period_and_offset(bus_aggregation_period, bus_aggregation_offset,
                                            other_bus_period, other_bus_offset)
    return bus_aggregation_offset, bus_aggregation_period


def get_start_sync_brut_force(bus_offset_dict):
    id_max, max_bus = get_max_bus(bus_offset_dict)
    time_start = max_bus - bus_offset_dict[max_bus]

    t = time_start
    while True:
        succession_found = 0
        for bus_number in bus_offset_dict.keys():
            if is_good_bus(t + bus_offset_dict[bus_number], bus_number):
                succession_found += 1
            else:
                break

        if succession_found == len(bus_offset_dict):
            break
        t += max_bus

    return t


def main():
    lines = read_file_as_lines("inputs/input-day13.txt")

    elapsed = int(lines[0])
    bus_instructions = get_buses(lines[1])
    bus_offset_dict = get_offset_dict(bus_instructions)
    print(bus_offset_dict)

    print(get_start_sync(bus_offset_dict))


if __name__ == "__main__":
    main()
