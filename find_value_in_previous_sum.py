from find_sum import find_matching_sum
from io_helper import read_file_as_lines, string_array_to_int_array


def get_min_max(integers):
    tmp_max = integers[0]
    tmp_min = integers[0]

    for integer in integers:
        if integer > tmp_max:
            tmp_max = integer
        if integer < tmp_min:
            tmp_min = integer

    return tmp_min, tmp_max


def find_contiguous_sum(integers, to_find):
    for i in range(len(integers) - 1):
        j = i + 1
        tmp_sum = integers[i]

        while j < len(integers) and tmp_sum < to_find:
            tmp_sum += integers[j]
            if tmp_sum == to_find:
                return integers[i: j]
            j += 1


def find_value_in_previous_sum(integers, buffer_length):
    checking_values = integers[:buffer_length]

    for i in range(buffer_length, len(integers)):

        if find_matching_sum(checking_values, integers[i]) == -1:
            return integers[i]

        checking_values.append(integers[i])
        checking_values = checking_values[1:]


def main():
    lines = read_file_as_lines("inputs/input-day9.txt")
    integers = string_array_to_int_array(lines)

    to_search = find_value_in_previous_sum(integers, 25)
    sum_elements = find_contiguous_sum(integers, to_search)
    sum_min, sum_max = get_min_max(sum_elements)
    print(sum_min + sum_max)


if __name__ == "__main__":
    main()
