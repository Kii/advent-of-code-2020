from io_helper import read_file_as_lines, group_lines_with_empty_line_separation


def main():
    lines = read_file_as_lines("inputs/input-day6.txt")
    all_grouped_lines = group_lines_with_empty_line_separation(lines)

    count_different_use = 0
    count_common_use = 0

    for grouped_lines in all_grouped_lines:
        inclusive_count = 0
        answer_dict = dict()

        for line in grouped_lines:
            for char in line:
                if not answer_dict.__contains__(char):
                    inclusive_count += 1
                    answer_dict[char] = 1
                else:
                    answer_dict[char] += 1

        exclusive_count = 0
        for k in answer_dict.keys():
            if answer_dict[k] == len(grouped_lines):
                exclusive_count += 1

        count_different_use += inclusive_count
        count_common_use += exclusive_count

    print(count_common_use)


if __name__ == "__main__":
    main()
