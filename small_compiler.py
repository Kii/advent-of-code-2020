from io_helper import read_file_as_lines


class Instruction:
    def __init__(self, function_name, value):
        self.function_name = function_name
        self.value = value


def parse_instruction(line):
    return Instruction(line.split(" ")[0], int(line.split(" ")[1]))


def get_instructions(lines):
    instructions = []
    for line in lines:
        instructions.append(parse_instruction(line))
    return instructions


def execute_instructions(instructions):
    i = 0
    acc = 0
    instructions_memory = dict()
    infinite_loop = True

    while infinite_loop and i < len(instructions):
        if not instructions_memory.__contains__(i):
            instructions_memory[i] = 1
            instr = instructions[i]
            if instr.function_name == "acc":
                acc += instr.value
                i += 1
            elif instr.function_name == "jmp":
                i += instr.value
            elif instr.function_name == "nop":
                i += 1
        else:
            return acc, infinite_loop

    return acc, False


def fix_instructions(instructions):
    for instr in instructions:
        if instr.function_name == "nop":
            instr.function_name = "jmp"
            acc, infinite = execute_instructions(instructions)
            if not infinite:
                return acc
            instr.function_name = "nop"
        elif instr.function_name == "jmp":
            instr.function_name = "nop"
            acc, infinite = execute_instructions(instructions)
            if not infinite:
                return acc
            instr.function_name = "jmp"


def main():
    lines = read_file_as_lines("inputs/input-day8.txt")
    instructions = get_instructions(lines)
    print(fix_instructions(instructions))

if __name__ == "__main__":
    main()
