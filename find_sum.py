from io_helper import read_file_as_lines, string_array_to_int_array


def is_correct_sum(t1, t2, result):
    return t1 + t2 == result


def find_matching_sum(number_list, result):
    size = len(number_list)
    i = 0
    while i < size - 1:
        t1 = number_list[i]
        j = i + 1
        while j < size:
            t2 = number_list[j]
            if is_correct_sum(t1, t2, result):
                return t1 * t2
            j += 1
        i += 1
    return -1


def find_triple_matching_sum(number_list, result):
    size = len(number_list)
    i = 0
    while i < size - 3:
        elem = number_list.pop(i)
        output = find_matching_sum(number_list, result-elem)
        if output != -1:
            return output*elem

        number_list.insert(i, elem)
        i += 1
    return -1


def main():
    lines = read_file_as_lines("inputs/input-day1.txt")
    print(find_triple_matching_sum(string_array_to_int_array(lines), 2020))


if __name__ == "__main__":
    main()
