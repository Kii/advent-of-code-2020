from io_helper import read_file_as_lines, group_lines_with_empty_line_separation


class Tile:
    def __init__(self, lines):
        id_line = lines[0]
        self.id = id_line.split(" ")[1][:-1]
        self.neighbor_ids = []
        self.lines = lines[1:]

        self.first_line = ""
        self.last_line = ""
        self.left_column = ""
        self.right_column = ""

        for i in range(1, len(lines)):
            line = lines[i]
            self.left_column += line[0]
            self.right_column += line[-1]
            if i == 1:
                for j in range(len(line)):
                    self.first_line += line[j]
            elif i == len(lines) - 1:
                for j in range(len(line)):
                    self.last_line += line[j]

    def rotate90(self):
        tmp_first_line = self.first_line
        self.first_line = self.left_column[::-1]
        self.left_column = self.last_line
        self.last_line = self.right_column[::-1]
        self.right_column = tmp_first_line

    def my_print(self):
        print("id: {}".format(self.id))
        for line in self.lines:
            print(line)

    def add_neighbor(self, other_id):
        self.neighbor_ids.append(other_id)

    def is_match(self, other):
        is_match = False
        for rotation in range(0, 4):
            if rotation != 0:
                other.rotate90()

            if self.first_line in [other.last_line, other.last_line[::-1]]:
                is_match = True
            if self.last_line in [other.first_line, other.first_line[::-1]]:
                is_match = True
            if self.left_column in [other.right_column, other.right_column[::-1]]:
                is_match = True
            if self.right_column in [other.left_column, other.left_column[::-1]]:
                is_match = True

        other.rotate90()
        return is_match


def main():
    lines = read_file_as_lines("inputs/input-day20.txt")
    grouped_lines = group_lines_with_empty_line_separation(lines)
    tile_map = dict()

    for tile_lines in grouped_lines:
        tile = Tile(tile_lines)
        tile_map[tile.id] = tile

    list_keys = list(tile_map.keys())
    for i in range(len(list_keys)):
        current_tile = tile_map[list_keys[i]]
        for j in range(len(list_keys)):
            if i != j:
                other_tile = tile_map[list_keys[j]]
                if current_tile.is_match(other_tile):
                    # print("Comparing {} and {}: MATCH".format(current_tile.id, other_tile.id))
                    current_tile.add_neighbor(other_tile.id)

    total = 1
    for i in range(len(list_keys)):
        current_tile = tile_map[list_keys[i]]
        if len(current_tile.neighbor_ids) == 2:
            print("tile: {}".format(current_tile.id))
            total *= int(current_tile.id)
    print(total)


if __name__ == "__main__":
    main()
