from find_value_in_previous_sum import get_min_max
from io_helper import read_file_as_lines


class GameState:
    def __init__(self, input_lines):
        self.all_points = dict()

        all_3d_spaces = dict()
        all_2d_shards = dict()

        for current_height in range(len(input_lines)):
            line = input_lines[current_height]
            point_line = dict()
            for current_width in range(len(line)):
                p = Point(current_width, current_height, 0, 0, line[current_width] == "#")
                point_line[current_width] = p
            all_2d_shards[current_height] = point_line

        all_3d_spaces[0] = all_2d_shards

        self.all_points[0] = all_3d_spaces

    def is_inside(self, point):
        try:
            points_3d = self.all_points[point.w]
            points_2d = points_3d[point.z]
            points_line = points_2d[point.y]
            return points_line.__contains__(point.x)
        except IndexError:
            return False

    def get_point_activity(self, point):
        try:
            points_3d = self.all_points[point.w]
            points_2d = points_3d[point.z]
            points_line = points_2d[point.y]
            point = points_line[point.x]
            return point.is_active
        except KeyError:
            return False

    def my_print(self, w=None):
        print("==========")

        all_spaces = self.all_points.keys()
        sorted_w = list(all_spaces)
        sorted_w.sort()

        if w is not None:
            sorted_w = [w]

        for w in sorted_w:
            space = self.all_points[w]
            sorted_z = list(space.keys())
            sorted_z.sort()
            for z in sorted_z:
                print("z: {} ; w: {}".format(z, w))
                shard = space[z]
                sorted_y = list(shard.keys())
                sorted_y.sort()
                for y in sorted_y:
                    line = shard[y]
                    str_line = ""
                    for x in line.keys():
                        str_line += "#" if line[x].is_active else "."
                    print("{}: {}".format(y, str_line))
                print("\n")

    def get_next_point_activity(self, point):
        point_neighbors = point.get_theoretical_neighbors_four_dimensions()

        count_old_neighbors_active = 0
        for old_point_neighbor in point_neighbors:
            if self.get_point_activity(old_point_neighbor):
                count_old_neighbors_active += 1

        if point.is_active:
            return count_old_neighbors_active in [2, 3]
        else:
            return count_old_neighbors_active == 3

    def new_line(self, y, z, w):
        new_line = dict()

        first_space = self.all_points[list(self.all_points.keys())[0]]
        first_shard = first_space[list(first_space.keys())[0]]
        first_line = first_shard[list(first_shard.keys())[0]]

        for key in first_line.keys():
            new_line[key] = Point(key, y, z, w)

        return new_line

    def new_shard(self, z, w):
        new_shard = dict()

        first_space = self.all_points[list(self.all_points.keys())[0]]
        first_shard = first_space[list(first_space.keys())[0]]

        for y_key in first_shard.keys():
            new_shard[y_key] = self.new_line(y_key, z, w)

        return new_shard

    def new_space(self, w):
        new_space = dict()
        first_space = self.all_points[list(self.all_points.keys())[0]]

        for z_keys in first_space.keys():
            new_space[z_keys] = self.new_shard(z_keys, w)

        return new_space

    def upsize(self):
        space_dict = self.all_points

        w_min, w_max = get_min_max(list(space_dict.keys()))
        for w in space_dict.keys():
            shard_dict = space_dict[w]

            z_min, z_max = get_min_max(list(shard_dict.keys()))
            for z in shard_dict.keys():
                line_dict = shard_dict[z]

                y_min, y_max = get_min_max(list(line_dict.keys()))
                for y in line_dict.keys():
                    column_dict = line_dict[y]

                    x_min, x_max = get_min_max(list(column_dict))
                    column_dict[x_min - 1] = Point(x_min - 1, y, z, w)
                    column_dict[x_max + 1] = Point(x_max + 1, y, z, w)
                line_dict[y_min - 1] = self.new_line(y_min - 1, z, w)
                line_dict[y_max + 1] = self.new_line(y_max + 1, z, w)
            shard_dict[z_min - 1] = self.new_shard(z_min - 1, w)
            shard_dict[z_max + 1] = self.new_shard(z_max + 1, w)
        space_dict[w_min - 1] = self.new_space(w_min - 1)
        space_dict[w_max + 1] = self.new_space(w_max + 1)

        self.all_points = space_dict

    def update(self):
        self.upsize()
        new_all_points = dict()
        for w in self.all_points.keys():
            space = self.all_points[w]
            new_space = dict()
            for z in space.keys():
                shard = space[z]
                new_shard = dict()
                for y in shard.keys():
                    line = shard[y]
                    new_line = dict()
                    for x in line.keys():
                        old_point = line[x]
                        new_activity = self.get_next_point_activity(old_point)

                        new_line[x] = Point(x, y, z, w, new_activity)
                    new_shard[y] = new_line
                new_space[z] = new_shard
            new_all_points[w] = new_space

        self.all_points = new_all_points

    def get_all_points(self):
        all_points = []
        for z in self.all_points.keys():
            shard = self.all_points[z]
            for y in shard.keys():
                line = shard[y]
                for x in line.keys():
                    all_points.append(line[x])
        return all_points

    def count_active_point(self):
        active_count = 0
        for w in self.all_points.keys():
            space = self.all_points[w]
            for z in space.keys():
                shard = space[z]
                for y in shard.keys():
                    line = shard[y]
                    for x in line.keys():
                        if line[x].is_active:
                            active_count += 1
        return active_count


class Point:
    def __init__(self, x, y, z, w=0, is_active=False):
        self.x = x
        self.y = y
        self.z = z
        self.w = w
        self.is_active = is_active

    def manhattan_distance(self, other_point):
        diff_x = abs(other_point.x - self.x)
        diff_y = abs(other_point.y - self.y)
        diff_z = abs(other_point.z - self.z)
        diff_w = abs(other_point.w - self.w)

        return diff_x + diff_y + diff_z + diff_w

    def get_theoretical_neighbors_three_dimensions(self):
        theoretical_neighbors = []
        for i in range(self.x - 1, self.x + 2):
            for j in range(self.y - 1, self.y + 2):
                for k in range(self.z - 1, self.z + 2):
                    if self.x != i or j != self.y or k != self.z:
                        theoretical_neighbors.append(Point(i, j, k))
        return theoretical_neighbors

    def get_theoretical_neighbors_four_dimensions(self):
        theoretical_neighbors = []
        for i in range(self.x - 1, self.x + 2):
            for j in range(self.y - 1, self.y + 2):
                for k in range(self.z - 1, self.z + 2):
                    for l in range(self.w - 1, self.w + 2):
                        if self.x != i or j != self.y or k != self.z or l != self.w:
                            theoretical_neighbors.append(Point(i, j, k, l))
        return theoretical_neighbors


def main():
    game_lines = [line.strip() for line in read_file_as_lines("inputs/input-day17.txt")]
    game = GameState(game_lines)
    game.my_print(0)

    for i in range(1):
        game.update()

    game.my_print()
    print(game.count_active_point())


if __name__ == "__main__":
    main()
