from io_helper import read_file_as_lines
from traverse_array import Position


def parse_instruction(instruction):
    return instruction[0], int(instruction[1:])


class Ship(Position):
    def __init__(self, x=0, y=0, facing="east"):
        super(Ship, self).__init__(x, y)
        self.facing = facing
        self.waypoint = Position(10, -1)

    def do_action(self, instruction):
        action, power = parse_instruction(instruction)
        if action in ["N", "S", "E", "W"]:
            if action == "N":
                self.waypoint.y -= power
            elif action == "S":
                self.waypoint.y += power
            elif action == "E":
                self.waypoint.x += power
            elif action == "W":
                self.waypoint.x -= power
        elif action in ["R", "L"]:
            simpler_power = int(power / 90) % 4
            for i in range(simpler_power):
                self.rotate_waypoint(action == "R")
        elif action == "F":
            direction_x = self.waypoint.x * power
            direction_y = self.waypoint.y * power
            self.x += direction_x
            self.y += direction_y

        else:
            raise ValueError("instruction not known " + instruction)

    def rotate_waypoint(self, clockwise):
        tmp_x = self.waypoint.x
        tmp_y = self.waypoint.y
        if not clockwise:
            self.waypoint.x = tmp_y
            self.waypoint.y = -1*tmp_x
        else:
            self.waypoint.x = -1*tmp_y
            self.waypoint.y = tmp_x

    def rotate(self, clockwise):
        if clockwise:
            if self.facing == "east":
                self.facing = "south"
            elif self.facing == "south":
                self.facing = "west"
            elif self.facing == "west":
                self.facing = "north"
            elif self.facing == "north":
                self.facing = "east"
        else:
            if self.facing == "east":
                self.facing = "north"
            elif self.facing == "north":
                self.facing = "west"
            elif self.facing == "west":
                self.facing = "south"
            elif self.facing == "south":
                self.facing = "east"


def main():
    instructions = read_file_as_lines("inputs/input-day12.txt")
    ship = Ship()

    for instruction in instructions:
        ship.do_action(instruction)

    print("(" + str(ship.x) + ";" + str(ship.y) + ")")
    print(ship.x + ship.y)


if __name__ == "__main__":
    main()
