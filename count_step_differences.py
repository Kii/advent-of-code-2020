from find_value_in_previous_sum import get_min_max
from io_helper import read_file_as_lines, string_array_to_int_array


def multiply_power_of_two(values):
    acc = 1
    for val in values:
        if val == 2:
            acc *= 2
        elif val == 3:
            acc *= 4
        elif val == 4:
            acc *= 7
    return acc


def get_length_of_consecutive(values, to_search):
    lengths = []
    i = 0
    while i < len(values):
        val = values[i]
        if val == to_search:
            consecutive_count = 1
            j = i
            while j < len(values) - 1:
                j += 1
                if values[j] == to_search:
                    consecutive_count += 1
                else:
                    i = j
                    break
            if consecutive_count > 1:
                lengths.append(consecutive_count)
        else:
            i += 1
    return lengths


def derive_values(values):
    tmp_val = 0
    derived = []
    for val in values:
        derived.append(val - tmp_val)
        tmp_val = val
    return derived


def get_step_distribution(values, maximal_step=3):
    jolt_input = 0
    diff_array = [0 for i in range(maximal_step)]
    for val in values:
        diff = val - jolt_input
        diff_array[diff - 1] += 1
        jolt_input = val
    return diff_array


def is_traversable(adapters, start_idx=0, maximal_step=3):
    jolt_input = 0
    if start_idx != 0:
        jolt_input = adapters[start_idx - 1]

    for i in range(start_idx, len(adapters)):
        adapter = adapters[i]
        diff = adapter - jolt_input
        if diff > maximal_step:
            return False
        jolt_input = adapter
    return True


def is_traversable_without(adapters_original, start_idx=0, maximal_step=3):
    if not is_traversable(adapters_original, start_idx, maximal_step):
        return 0
    else:
        total_count = 1
        adapters = adapters_original
        for i in range(start_idx, len(adapters) - 1):
            elem = adapters.pop(i)
            total_count += is_traversable_without(adapters, i)
            adapters.insert(i, elem)
            i += 1
        return total_count


def main():
    lines = read_file_as_lines("inputs/input-day10.txt")
    adapters = string_array_to_int_array(lines)
    adapters.sort()

    jolt_min, jolt_max = get_min_max(adapters)
    adapters.append(jolt_max + 3)

    steps = get_step_distribution(adapters)
    print("step_diff1 = " + str(steps[0]))
    print("step_diff2 = " + str(steps[1]))
    print("step_diff3 = " + str(steps[2]))
    print("step_diff1 * step_diff3 = " + str(steps[0] * steps[2]))
    print(is_traversable(adapters))

    print(derive_values(adapters))
    print(get_length_of_consecutive(derive_values(adapters), 1))
    print(multiply_power_of_two(get_length_of_consecutive(derive_values(adapters), 1)))


if __name__ == "__main__":
    main()
