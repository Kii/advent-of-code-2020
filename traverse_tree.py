from io_helper import read_file_as_lines


def parse_line(line):
    # posh blue bags contain 5 plaid chartreuse bags, 3 plaid lime bags.
    line_split = line.split(" contain ")
    ancestor_part = line_split[0]
    children_part = line_split[1]

    ancestor_words = ancestor_part.split(" ")
    ancestor = " ".join(ancestor_words[:len(ancestor_words) - 1])

    children_dict = dict()

    children_bags = children_part.split(", ")
    for children_bag in children_bags:
        child_words = children_bag.split(" ")
        try:
            number = int(child_words[0])
            color = " ".join(child_words[1:len(child_words) - 1])
            children_dict[color] = number
        except ValueError:
            pass

    return ancestor, children_dict


def generate_dict_from_rules(lines):
    ancestor_to_children = dict()
    child_to_ancestors = dict()

    for line in lines:
        ancestor, children_dict = parse_line(line)

        if not ancestor_to_children.__contains__(ancestor):
            ancestor_to_children[ancestor] = children_dict

        for child in children_dict.keys():
            if not child_to_ancestors.__contains__(child):
                child_to_ancestors[child] = []
            child_to_ancestors[child].append(ancestor)

    return ancestor_to_children, child_to_ancestors


def count_ancestors(child_to_ancestor_dict, color, explored_list=None):
    count = 0
    already_explored = explored_list
    if already_explored is None:
        already_explored = dict()

    if child_to_ancestor_dict.__contains__(color):
        for ancestor in child_to_ancestor_dict[color]:
            if not already_explored.__contains__(ancestor):
                count += 1
                already_explored[ancestor] = 1
                count += count_ancestors(child_to_ancestor_dict, ancestor, already_explored)

    return count


def count_children(ancestor_to_dict_child_dict, color, children_count_dict=None):
    count = 1

    if children_count_dict is None:
        count = 0
        children_count_dict = dict()

    if ancestor_to_dict_child_dict.__contains__(color):
        children_dict = ancestor_to_dict_child_dict[color]
        if len(children_dict) > 0:
            for child_color in children_dict.keys():
                multiplier = children_dict[child_color]
                if children_count_dict.__contains__(child_color):
                    count += multiplier * children_count_dict[child_color]
                else:
                    nb_bags_inside = count_children(ancestor_to_dict_child_dict, child_color, children_count_dict)
                    children_count_dict[child_color] = nb_bags_inside
                    count += multiplier * nb_bags_inside

    return count


def main():
    lines = read_file_as_lines("inputs/input-day7.txt")
    ancestor_to_children_dict, child_to_ancestor_dict = generate_dict_from_rules(lines)
    ancestor_count = count_ancestors(child_to_ancestor_dict, "shiny gold")
    child_count = count_children(ancestor_to_children_dict, "shiny gold")

    print("shiny gold ancestors: " + str(ancestor_count))
    print("shiny gold children: " + str(child_count))


if __name__ == "__main__":
    main()
