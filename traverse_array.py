from io_helper import read_file_as_lines


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def get_all_directions():
    return [
        Position(0, 1),
        Position(0, -1),
        Position(1, 0),
        Position(-1, 0),
        Position(-1, -1),
        Position(1, 1),
        Position(-1, 1),
        Position(1, -1),
    ]


def traverse_array(game_map, direction):
    current_position = Position(direction.x, direction.y)
    height = len(game_map)
    width = len(game_map[0]) - 1

    count_trees = 0
    count_free_spaces = 0
    count_steps = 0

    while current_position.y < height:
        item = game_map[current_position.y][current_position.x]
        if item == ".":
            count_free_spaces += 1
        elif item == "#":
            count_trees += 1

        current_position.x += direction.x
        current_position.y += direction.y

        current_position.x = current_position.x % width

        count_steps += 1

    return count_trees, count_free_spaces, count_steps


def main():
    game_map = read_file_as_lines("inputs/input-day3.txt")
    directions = [Position(1, 1),
                  Position(3, 1),
                  Position(5, 1),
                  Position(7, 1),
                  Position(1, 2)]

    multiply = 1

    for direction in directions:
        trees = traverse_array(game_map, direction)
        multiply *= trees[0]

    print(multiply)


if __name__ == "__main__":
    main()
