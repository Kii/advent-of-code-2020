from io_helper import read_file_as_lines, group_lines_with_empty_line_separation


class Rule:
    def __init__(self, name, min_max_tuples):
        self.name = name
        self.min_max_tuples = min_max_tuples

    def match(self, value):
        value_match_rule = False
        for min_max_tuple in self.min_max_tuples:
            if min_max_tuple.min_val <= value <= min_max_tuple.max_val:
                value_match_rule = True

        return value_match_rule


class MinMaxTuple:
    def __init__(self, min_val, max_val):
        self.min_val = int(min_val)
        self.max_val = int(max_val)


def parse_rule_line(line):
    # line example :
    # departure location: 32-842 or 854-967
    key_value = line.split(":")
    key = key_value[0].strip()

    tuples = []

    # values example :
    # 32-842 or 854-967
    values = key_value[1].strip()
    value_dynamics = values.split(" or ")
    for dynamic in value_dynamics:
        min_max = dynamic.strip().split("-")
        min_dynamic = min_max[0]
        max_dynamic = min_max[1]
        tuples.append(MinMaxTuple(min_dynamic, max_dynamic))

    return Rule(key, tuples)


def each_value_match_at_least_one_rule(values, rules):
    for value in values:
        value_match_one_rule = False
        for rule in rules:
            if rule.match(value):
                value_match_one_rule = True
                break
        if not value_match_one_rule:
            return False

    return True


def get_values_not_matching_any_rules(values, rules):
    not_matching_values = []
    for value in values:
        value_match_one_rule = False
        for rule in rules:
            if rule.match(value):
                value_match_one_rule = True
                break
        if not value_match_one_rule:
            not_matching_values.append(value)

    return not_matching_values


def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


def get_matching_rule_names(value, rules):
    matching_rules = []
    for rule in rules:
        if rule.match(value):
            matching_rules.append(rule.name)
    return matching_rules


def remove_rule_name_from_other_rule_positions(position_valid_rules, position, rule_name):
    for i in range(len(position_valid_rules)):
        if i != position:
            other_rules = position_valid_rules[i]
            if other_rules.__contains__(rule_name):
                other_rules.remove(rule_name)
    return position_valid_rules


def main():
    grouped_lines = group_lines_with_empty_line_separation(read_file_as_lines("inputs/input-day15.txt"))

    rule_lines = grouped_lines[0]
    rules = [parse_rule_line(line) for line in rule_lines]

    my_ticket_lines = grouped_lines[1]
    my_ticket_values = [int(val) for val in my_ticket_lines[1].split(",")]

    position_valid_rules = dict()
    for i in range(len(my_ticket_values)):
        my_ticket_value = my_ticket_values[i]
        position_valid_rules[i] = get_matching_rule_names(my_ticket_value, rules)

    other_ticket_lines = grouped_lines[2]
    valid_ticket_lines = []
    for i in range(1, len(other_ticket_lines)):
        ticket_line = other_ticket_lines[i]
        ticket_values = [int(val) for val in ticket_line.split(",")]
        if each_value_match_at_least_one_rule(ticket_values, rules):
            valid_ticket_lines.append(ticket_line)

    for valid_ticket_line in valid_ticket_lines:
        ticket_values = [int(val) for val in valid_ticket_line.split(",")]
        for i in range(len(ticket_values)):
            value = ticket_values[i]
            previously_matched_rules = position_valid_rules[i]
            matching_rules_names = get_matching_rule_names(value, rules)

            position_valid_rules[i] = intersection(matching_rules_names, previously_matched_rules)

    not_clean = True
    while not_clean:
        all_clean = True
        for position in position_valid_rules.keys():
            if len(position_valid_rules[position]) == 1:  # only one rule for this position
                # remove this rule for other positions
                rule_name = position_valid_rules[position][0]
                remove_rule_name_from_other_rule_positions(position_valid_rules, position, rule_name)
            else:
                all_clean = False

        if all_clean:
            not_clean = False

    result = 1
    for position in position_valid_rules.keys():
        rule_name = position_valid_rules[position][0]
        if rule_name.startswith("departure"):
            print("{}: {}", position, rule_name)
            result *= my_ticket_values[position]

    print(result)


if __name__ == "__main__":
    main()
