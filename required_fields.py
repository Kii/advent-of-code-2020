from io_helper import read_file_as_lines, group_lines_with_empty_line_separation


def required_fields():
    return ["byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid"]


def is_valid_field(field_name, field_value):
    try:
        if field_name == "byr":
            return len(field_value) == 4 and (1920 <= int(field_value) <= 2002)
        elif field_name == "iyr":
            return len(field_value) == 4 and (2010 <= int(field_value) <= 2020)
        elif field_name == "eyr":
            return len(field_value) == 4 and (2020 <= int(field_value) <= 2030)
        elif field_name == "hgt":
            if "cm" in field_value:
                return len(field_value) == 5 and (150 <= int(field_value[:3]) <= 193)
            elif "in" in field_value:
                return len(field_value) == 4 and (59 <= int(field_value[:2]) <= 76)
            else:
                return False
        elif field_name == "hcl":
            return len(field_value) == 7 and int(field_value[1:], 16)
        elif field_name == "ecl":
            return field_value in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        elif field_name == "pid":
            return len(field_value) == 9 and int(field_value)
        elif field_name == "cid":
            return True
        else:
            return False
    except ValueError:
        return False


def is_valid_credentials(group_lines):
    group_fields = []
    for line in group_lines:
        for field in line.split(" "):
            group_fields.append(field)

    mandatory_fields = required_fields()
    for field in group_fields:
        field_key = field.split(":")[0]
        if field_key in mandatory_fields:
            if is_valid_field(field_key, field.split(":")[1]):
                mandatory_fields.remove(field_key)

    return len(mandatory_fields) == 0


def main():
    lines = read_file_as_lines("inputs/input-day4.txt")

    all_grouped_lines = group_lines_with_empty_line_separation(lines)
    valid_credentials = 0

    for grouped_lines in all_grouped_lines:
        if is_valid_credentials(grouped_lines):
            valid_credentials += 1

    print(valid_credentials)


if __name__ == "__main__":
    main()
