from io_helper import read_file_as_lines, group_lines_with_empty_line_separation


def parse_line(line):
    splits = line.split(":")
    key = int(splits[0].strip())
    value = splits[1].strip()
    return key, value


def parse_rules_to_dict(lines):
    rule_dict = dict()

    for line in lines:
        key, value = parse_line(line)
        rule_dict[key] = value

    return rule_dict


def is_real_value(message):
    return message.__contains__('"')


def is_conditional_rule(message):
    return message.__contains__('|')


def get_real_rule(rules_dict, current_rule):
    if is_real_value(current_rule):
        return [current_rule[1]]

    if is_conditional_rule(current_rule):
        rules = current_rule.split("|")
        split_rules = list()
        for rule in get_real_rule(rules_dict, rules[0].strip()):
            split_rules.append(rule)
        for rule in get_real_rule(rules_dict, rules[1].strip()):
            split_rules.append(rule)
        return split_rules

    output_rules = [""]

    parsed_rules = current_rule.split(" ")
    for parse_rule in parsed_rules:
        key = int(parse_rule.strip())
        key_rules = get_real_rule(rules_dict, rules_dict[key])

        output_rule_length = len(output_rules)
        for num_key_rule in range(1, len(key_rules)):
            key_rule = key_rules[num_key_rule]
            for num_output_rule in range(output_rule_length):
                new_val = output_rules[num_output_rule] + key_rule
                output_rules.append(new_val)

        for num_output_rule in range(output_rule_length):
            new_val = output_rules[num_output_rule] + key_rules[0]
            output_rules[num_output_rule] = new_val

    return output_rules


def check_match(rules, message):
    return message in rules


def check_real_match(rules_dict, message):
    rule42 = get_real_rule(rules_dict, rules_dict[42])
    rule31 = get_real_rule(rules_dict, rules_dict[31])

    buffer_len = len(rule42[0])
    message_len = len(message)

    count_rule42 = 0
    count_rule31 = 0

    for i in range(int(message_len / buffer_len)):
        message_part = message[i * buffer_len:(i + 1) * buffer_len]

        if message_part in rule42:
            count_rule42 += 1
            if count_rule31 > 0:
                return False

        elif message_part in rule31:
            if count_rule42 < 2:
                return False
            count_rule31 += 1

        else:
            return False

    return (count_rule42 > count_rule31) and count_rule31 > 0


def main():
    lines = read_file_as_lines("inputs/input-day19.txt")
    grouped_lines = group_lines_with_empty_line_separation(lines)

    rules = grouped_lines[0]
    messages = grouped_lines[1]

    count_msg = 0

    rules_dict = parse_rules_to_dict(rules)

    for message in messages:
        if check_real_match(rules_dict, message):
            count_msg += 1
        else:
            print("not valid: {}".format(message))

    print(count_msg)


if __name__ == "__main__":
    main()
