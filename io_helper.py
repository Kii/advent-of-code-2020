def read_file_as_lines(filename):
    with open(filename, 'r') as file:
        return file.readlines()


def group_lines_with_empty_line_separation(lines):
    grouped_lines = []
    tmp_group = []

    for i in range(len(lines)):
        line = lines[i].strip()
        if line == "":
            grouped_lines.append(tmp_group)
            tmp_group = []
        elif i == len(lines) - 1:
            tmp_group.append(line)
            grouped_lines.append(tmp_group)
        else:
            tmp_group.append(line)

    return grouped_lines


def string_array_to_int_array(lines):
    return [int(numeric_string) for numeric_string in lines]
