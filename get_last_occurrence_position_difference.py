def init_values():
    last_position_dict = dict()
    values = [12, 20, 0, 6, 1, 17, 7]

    for i in range(len(values)):
        print("{}: {}".format(i, values[i]))
        next_to_add = get_next_value(values[i], last_position_dict, i)
        last_position_dict[values[i]] = i

    return last_position_dict, next_to_add


def get_next_value(value, last_position_dict, current_position):
    if last_position_dict.__contains__(value):
        age = current_position - last_position_dict[value]
        return age
    else:
        return 0


def guess_next_number(last_position_dict, next_to_add, begin_iteration, nb_iteration=2020):
    last_val = 0
    for i in range(begin_iteration, nb_iteration):
        # print("{}: {}".format(i, next_to_add))
        future_val = get_next_value(next_to_add, last_position_dict, i)
        last_position_dict[next_to_add] = i

        last_val = next_to_add
        next_to_add = future_val

    return last_val


def main():
    last_position_dict, next_to_add = init_values()
    print(guess_next_number(last_position_dict, next_to_add, len(last_position_dict), 30000000))


if __name__ == "__main__":
    main()
