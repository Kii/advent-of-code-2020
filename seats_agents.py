from io_helper import read_file_as_lines
from traverse_array import get_all_directions


def is_seat(item):
    return item == "L" or item == "#"


def is_empty_seat(item):
    return item == "L"


def is_occupied_seat(item):
    return item == "#"


def is_inside(game_lines, col_num, row_num):
    if 0 <= row_num < len(game_lines):
        if 0 <= col_num < len(game_lines[row_num]):
            return True
    return False


def get_nearest_seats(game_lines, col_num, row_num):
    seats = []

    directions = get_all_directions()

    for direction in directions:
        col = col_num + direction.x
        row = row_num + direction.y
        while is_inside(game_lines, col, row):
            if is_seat(game_lines[row][col]):
                seats.append(game_lines[row][col])
                break
            else:
                col += direction.x
                row += direction.y

    return seats


def get_neighbors(game_lines, col_num, row_num):
    neighbors = []
    for row in range(row_num - 1, row_num + 2):
        for col in range(col_num - 1, col_num + 2):
            if is_inside(game_lines, col, row) and (row != row_num or col != col_num):
                neighbors.append(game_lines[row][col])

    return neighbors


def count_occupied_seats(game_lines):
    count_occupied = 0
    for row in range(len(game_lines)):
        current_line = game_lines[row]
        for col in range(len(current_line)):
            if is_occupied_seat(current_line[col]):
                count_occupied += 1
    return count_occupied


def update_item(item, game_lines, col, row):
    if is_empty_seat(item):
        no_occupied_neighbor = True
        for neighbor in get_nearest_seats(game_lines, col, row):
            if is_occupied_seat(neighbor):
                no_occupied_neighbor = False
        if no_occupied_neighbor:
            return "#"
        else:
            return "L"
    elif is_occupied_seat(item):
        nb_occupied_neighbor = 0
        for neighbor in get_nearest_seats(game_lines, col, row):
            if is_occupied_seat(neighbor):
                nb_occupied_neighbor += 1
        if nb_occupied_neighbor >= 5:
            return "L"
        else:
            return "#"
    else:
        return item


def do_game_loop(game_lines):
    next_game_lines = None
    current_game_lines = game_lines
    while True:
        if next_game_lines is not None:
            current_game_lines = next_game_lines
        next_game_lines = []
        count_change = 0
        for row in range(len(current_game_lines)):
            next_line = []
            current_line = current_game_lines[row]
            for col in range(len(current_line)):
                item = current_line[col]
                updated_item = update_item(item, current_game_lines, col, row)
                next_line.append(updated_item)
                if item != updated_item:
                    count_change += 1

            next_game_lines.append(next_line)

        if count_change == 0:
            break

    return next_game_lines


def init_game(game_lines):
    height = len(game_lines)
    width = len(game_lines[0])

    current_game_lines = []
    for i in range(height):
        current_line = []
        for j in range(width):
            current_line.append(game_lines[i][j])
        current_game_lines.append(current_line)

    return current_game_lines


def main():
    game_lines = [line.strip() for line in read_file_as_lines("inputs/input-day11.txt")]
    current_game_lines = init_game(game_lines)
    [print(line) for line in current_game_lines]
    stable_game_lines = do_game_loop(current_game_lines)
    [print(line) for line in stable_game_lines]
    count_occupied_seat = count_occupied_seats(stable_game_lines)
    print(count_occupied_seat)


if __name__ == "__main__":
    main()
