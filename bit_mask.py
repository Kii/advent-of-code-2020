from io_helper import read_file_as_lines


def get_zeros(number):
    zeros = ""
    for i in range(number):
        zeros += "0"
    return zeros


class Memory:
    def __init__(self):
        self.addresses_dict = dict()
        self.current_mask_str = get_zeros(36)

    def update_mask(self, mask_str):
        self.current_mask_str = mask_str

    def get_masked_value(self, decimal_value):
        binary_value_str = bin(decimal_value)[2:]
        reversed_bit_value_str = binary_value_str[::-1]
        reversed_mask_str = self.current_mask_str[::-1]

        masked_value = ""
        for i in range(len(reversed_mask_str)):
            mask_bit = reversed_mask_str[i]
            if mask_bit == "X":
                if i < len(reversed_bit_value_str):
                    masked_value += reversed_bit_value_str[i]
                else:
                    masked_value += "0"
            else:
                masked_value += reversed_mask_str[i]

        return int(str(masked_value[::-1]), 2)

    def get_masked_address(self, decimal_address):
        binary_address_str = bin(decimal_address)[2:]
        reversed_bit_value_str = binary_address_str[::-1]
        reversed_mask_str = self.current_mask_str[::-1]

        all_addresses = [""]

        for i in range(len(reversed_mask_str)):
            mask_bit = reversed_mask_str[i]
            address_count = len(all_addresses)
            for j in range(address_count):
                if mask_bit == "X":
                    all_addresses.append(all_addresses[j]+"1")
                    all_addresses[j] += "0"
                elif mask_bit == "1":
                    all_addresses[j] += "1"
                else:
                    if i < len(reversed_bit_value_str):
                        all_addresses[j] += reversed_bit_value_str[i]
                    else:
                        all_addresses[j] += "0"

        all_new_addresses = []
        for address in all_addresses:
            new_address = int(str(address[::-1]), 2)
            all_new_addresses.append(new_address)

        return all_new_addresses

    def update_address(self, address_str, str_decimal_value):
        masked_value = self.get_masked_value(int(str_decimal_value))
        self.addresses_dict[address_str] = self.get_masked_value(masked_value)

    def update_address2(self, address_str, str_decimal_value):
        all_addresses = self.get_masked_address(int(address_str))
        for address in all_addresses:
            self.addresses_dict[address] = int(str_decimal_value)


def update_memory(lines):
    mem = Memory()
    for line in lines:
        args = [opt.strip() for opt in line.split("=")]
        if args[0] == "mask":
            mask_str = args[1]
            mem.update_mask(mask_str)
        else:
            addr_str = args[0][4:len(args[0]) - 1]
            value_str = args[1]
            mem.update_address2(addr_str, value_str)

    return mem


def main():
    lines = read_file_as_lines("inputs/input-day14.txt")
    mem = update_memory(lines)
    total = 0
    for addr_str in mem.addresses_dict.keys():
        total += mem.addresses_dict[addr_str]

    print(total)


if __name__ == "__main__":
    main()
