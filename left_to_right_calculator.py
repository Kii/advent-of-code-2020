from io_helper import read_file_as_lines


def get_operator_indices(line, operators=None):
    indices = []
    look_for_operators = ["+", "*"]
    if operators is not None:
        look_for_operators = operators

    for i in range(len(line)):
        if line[i] in look_for_operators:
            indices.append(i)
    return indices


def is_simplest_expression(line):
    return not line.__contains__("(")


def is_simple_enough_expression(line):
    all_operator_count = len(get_operator_indices(line, ["+", "*"]))
    priority_operator_count = len(get_operator_indices(line, ["+"]))

    return all_operator_count == 1 or priority_operator_count == all_operator_count or priority_operator_count == 0


def get_result(left, operator, right):
    if operator.strip() == "+":
        return int(left) + int(right)
    elif operator.strip() == "*":
        return int(left) * int(right)
    else:
        raise ValueError


def get_next_number_index_end(line, id):
    i = id + 2
    while i < len(line):
        m_char = line[i]
        if not m_char.isnumeric():
            break
        i += 1
    return i


def get_previous_number_index_start(line, idx):
    i = idx - 2
    while i >= 0:
        m_char = line[i]
        if not m_char.isnumeric():
            break
        i -= 1
    return i + 1


def add_brackets(line):
    if not is_simplest_expression(line):  # cannot simplify more
        raise ValueError(line)
    if is_simple_enough_expression(line):
        raise ValueError(line)

    idx = 0
    offset = 0
    new_line = str(line)

    operator_indices = get_operator_indices(line)

    while idx < len(operator_indices):
        i = operator_indices[idx]
        operator = line[i]

        if operator == "+":
            if (idx == 0) or line[operator_indices[idx - 1]] != "+":
                previous = get_previous_number_index_start(line, i)
                start = new_line[:previous + offset]
                end = new_line[previous + offset:]
                new_line = start + "(" + end
                offset += 1

            if (idx == len(operator_indices) - 1) or line[operator_indices[idx + 1]] != "+":
                successor = get_next_number_index_end(line, i)
                start = new_line[:successor + offset]
                end = new_line[successor + offset:]
                new_line = start + ")" + end
                offset += 1

        idx += 1

    return new_line  # .replace("()", "")


def get_real_value(line):
    if not is_simplest_expression(line):  # cannot simplify more
        raise ValueError(line)
    if not is_simple_enough_expression(line):
        raise ValueError(line)

    i = 0
    idx = get_operator_indices(line)
    while i < len(idx):
        if i == 0:
            previous_id = 0
            if len(idx) == 1:
                next_id = len(line)
            else:
                next_id = idx[i + 1]
        elif i == len(idx) - 1:
            previous_id = idx[i - 1] + 1
            next_id = len(line)
        else:
            previous_id = idx[i - 1] + 1
            next_id = idx[i + 1]

        if i == 0:
            left = line[previous_id:idx[i]]
            operator = line[idx[i]]
            right = line[idx[i] + 1:next_id]
        else:
            left_value = get_result(left, operator, right)
            left = str(left_value)
            operator = line[idx[i]]
            right = line[idx[i] + 1:next_id]

        i += 1

    return get_result(left, operator, right)


def calculate_value(line):
    # example : 4 + ((8 + 3 * 6) * 7 * 4)

    bracket_stack = []
    str_buffer = ""

    for i in range(len(line)):
        str_buffer += line[i]

        if line[i] == "(":
            bracket_stack.append(str_buffer[:-1])
            str_buffer = ""

        elif line[i] == ")":
            value_str = str_buffer[:-1]

            if is_simplest_expression(value_str):
                if is_simple_enough_expression(value_str):
                    value_str = str(get_real_value(value_str))
                else:
                    prioritized_value_str = add_brackets(value_str)
                    value_str = str(calculate_value(prioritized_value_str))

            str_buffer = bracket_stack.pop() + value_str

    if is_simplest_expression(str_buffer):
        if is_simple_enough_expression(str_buffer):
            return get_real_value(str_buffer)
        else:
            prioritized_buffer_str = add_brackets(str_buffer)
            return calculate_value(prioritized_buffer_str)
    else:
        raise ValueError(str_buffer)


def main():
    lines = read_file_as_lines("inputs/input-day18.txt")
    sum_values = 0
    for line in lines:
        print(line)
        sum_values += calculate_value(line.strip())
    print(sum_values)


if __name__ == "__main__":
    main()
