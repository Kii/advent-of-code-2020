from math import sqrt


def is_prime(number):
    if number % 2 == 0:
        return False
    i = 3
    while i <= int(sqrt(number)):
        if number % i == 0:
            return False
        i += 2
    return True


def pgcd(a, b):
    if b == 0:
        return a
    else:
        r = a % b
        return pgcd(b, r)


def sync_modulo(a, b):
    return int((a * b) / pgcd(a, b))


def get_aggregate_period_and_offset(period1, offset1, period2, offset2):
    if period1 < period2:
        return get_aggregate_period_and_offset(period2, offset2, period1, offset1)

    nb_found = 0
    i = 0
    first_pos = 0
    period = 0
    while nb_found < 2:
        position = (period1 * i) + offset1
        if (position + offset2) % period2 == 0:
            nb_found += 1
            if nb_found == 1:
                first_pos = position
            elif nb_found == 2:
                period = position - first_pos

        i += 1
    return period, first_pos


def get_divisors(number):
    divisors = []
    if number <= 1:
        return divisors
    elif number == 1:
        return divisors
    elif number == 2:
        divisors.append(2)
        return divisors

    i = 2
    while i <= int(sqrt(number)):
        if number % i == 0:
            divisors.append(i)
            if i != int(number / i):
                divisors.append(int(number / i))
        else:
            break
        i += 2

    i = 3
    while i <= int(sqrt(number)):
        if number % i == 0:
            divisors.append(i)
            if i != int(number / i):
                divisors.append(int(number / i))
        i += 2

    divisors.sort()

    return divisors


def main():
    print(is_prime(13))
    print(get_divisors(601080743))
    print(pgcd(3, 7))

    print(sync_modulo(sync_modulo(3, 7), 11))
    print(get_aggregate_period_and_offset(13, 0, 11, 1))


if __name__ == "__main__":
    main()
