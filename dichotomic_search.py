from io_helper import read_file_as_lines


def get_first_half(min_val, max_val):
    return min_val, min_val + int((max_val - min_val) / 2)


def get_second_half(min_val, max_val):
    return (min_val + int((max_val - min_val) / 2)) + 1, max_val


def find_missing_values(val_array):
    missing_values = []
    for i in range(len(val_array) - 1):
        if val_array[i + 1] - val_array[i] != 1:
            missing_values.append(val_array[i] + 1)

    return missing_values


def find_row(min_row, max_row, instructions):
    for instr in instructions:
        if instr == "F":
            min_row, max_row = get_first_half(min_row, max_row)
        elif instr == "B":
            min_row, max_row = get_second_half(min_row, max_row)
    if min_row == max_row:
        return min_row
    else:
        raise ValueError


def find_col(min_col, max_col, instructions):
    for instr in instructions:
        if instr == "L":
            min_col, max_col = get_first_half(min_col, max_col)
        elif instr == "R":
            min_col, max_col = get_second_half(min_col, max_col)
    if min_col == max_col:
        return min_col
    else:
        raise ValueError


def main():
    lines = read_file_as_lines("inputs/input-day5.txt")

    max_seat_id = 0
    available_seats = []

    for line in lines:
        row = find_row(0, 127, line[:7])
        col = find_col(0, 7, line[7:])
        seat_id = (row * 8) + col
        available_seats.append(seat_id)

        if seat_id > max_seat_id:
            max_seat_id = seat_id

    print("max ID:" + str(max_seat_id))

    available_seats.sort()
    print("Missing ids: " + str(find_missing_values(available_seats)))


if __name__ == "__main__":
    main()
