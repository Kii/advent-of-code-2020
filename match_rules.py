from io_helper import read_file_as_lines


def parse_min_max_expr(expr):
    min_max = expr.split("-")
    return int(min_max[0]), int(min_max[1])


def parse_rules_from_line(line):
    args = line.split(" ")
    min_max_expr = args[0]
    min_expr, max_expr = parse_min_max_expr(min_max_expr)
    letter_constraint_expr = args[1]
    must_match_letter = letter_constraint_expr[0]
    password = args[2]
    return PasswordRule(min_expr, max_expr, must_match_letter), password


def is_valid_position(position, length):
    return 0 <= position - 1 < length


class PasswordRule:
    def __init__(self, min_occ, max_occ, letter):
        self.min = min_occ
        self.max = max_occ
        self.letter = letter

    def is_valid(self, password):
        occ = password.count(self.letter)
        return self.min <= occ <= self.max

    def is_really_valid(self, password):
        if not is_valid_position(self.min, len(password)):
            return False
        if not is_valid_position(self.max, len(password)):
            return False
        acc = 0
        if password[self.min - 1] == self.letter:
            acc += 1
        if password[self.max - 1] == self.letter:
            acc += 1
        return acc == 1


def main():
    lines = read_file_as_lines("inputs/input-day2.txt")
    valid_passwords = 0
    for line in lines:
        rules, password = parse_rules_from_line(line)
        if rules.is_really_valid(password):
            valid_passwords += 1

    print("Valid passwords : " + str(valid_passwords))


if __name__ == "__main__":
    main()
